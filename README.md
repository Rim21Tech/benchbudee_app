# README #

This REPO contains the files used in the design and development of the BenchBudEE App - iOS version. The App is used in conjunction with the Contextual Electronics course Spring 2014.

### What is this repository for? ###

* Contains the files include all the 'practice' Apps with the final version named BenchBudEE
* This is the 1st version of the App
* [Contextual Electronics](http://contextualelectronics.com/)

The Design Goal for the App is this:
![BenchBudEE_SettingsAdjust.jpg](https://bitbucket.org/repo/oryrMG/images/2515665618-BenchBudEE_SettingsAdjust.jpg)


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines