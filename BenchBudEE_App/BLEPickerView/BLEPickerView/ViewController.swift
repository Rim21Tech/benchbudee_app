//
//  ViewController.swift
//  BLEPickerView
//
//  Created by Rim21 on 24/11/2014.
//  Copyright (c) 2014 Nathan Rima. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    
    @IBOutlet weak var BLEUpdate: UILabel!
    @IBOutlet weak var pickerBgndBottom: UIImageView!
    @IBOutlet weak var pickerBgndTop: UIImageView!
    @IBOutlet weak var tempGauge: UIImageView!
    @IBOutlet weak var mainDisplay: UIImageView!
    @IBOutlet weak var metalBgnd: UIImageView!
    @IBOutlet weak var BLEStart: UIButton!
    @IBOutlet weak var BLEStatus: UIButton!
    @IBOutlet weak var BLESelector: UIPickerView!
    
    @IBAction func BLEOn(sender: UIButton) {
        
    }
    
    let BLEName = ["88888888", "55555555", "11111111", "44444444", "22222222", "33333333", "66666666", "77777777"]
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BLESelector.dataSource = self
        BLESelector.delegate = self
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        //PickerView custom settings
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return BLEName.count
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        return BLEName[row]
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        BLEUpdate.text = BLEName[row]
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = BLEName[row]
        var myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 12.0)!,NSForegroundColorAttributeName:UIColor.blueColor()])
        return myTitle
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var pickerLabel = view as UILabel!
        if view == nil {  //if no label there yet
            pickerLabel = UILabel()
            pickerLabel.textAlignment = .Center
        }
        let titleData = BLEName[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 12.0)!,NSForegroundColorAttributeName:UIColor.blackColor()])
        pickerLabel!.attributedText = myTitle
        
        return pickerLabel
    }



}

