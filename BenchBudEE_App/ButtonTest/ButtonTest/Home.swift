//
//  Home.swift
//  ButtonTest
//
//  Created by Rim21 on 7/09/2014.
//  Copyright (c) 2014 Nathan Rima. All rights reserved.
//

import UIKit

class Home: UIViewController {
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var g100Button: UIButton!
    @IBOutlet weak var bypassButtonNeg: UIButton!
    @IBOutlet weak var adcButton: UIButton!
    @IBOutlet weak var ledButton: UIButton!
    @IBOutlet weak var dacButton: UIButton!
    @IBOutlet weak var relayButton: UIButton!
    
    @IBAction func enterBtnPressed(sender: AnyObject) {
        println("Enter button pressed")
    }
    
    @IBAction func g100BtnPressed(sender: AnyObject) {
        println("G100 button pressed")
    }
    
    @IBAction func bypassBtnNegPressed(sender: AnyObject) {
        println("Bypass button pressed")
    }
    
    @IBAction func ADCBtnPressed(sender: AnyObject) {
        println("ADC button pressed")
    }
    
    @IBAction func LEDBtnPressed(sender: AnyObject) {
        println("LED button pressed")
    }
    
    @IBAction func DACBtnPressed(sender: AnyObject) {
        println("DAC button pressed")
    }
    
    @IBAction func relayBtnPressed(sender: AnyObject) {
        println("Relay button pressed")
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

}
