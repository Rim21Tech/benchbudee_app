//
//  ViewController.swift
//  displayPractice
//
//  Created by Rim21 on 13/09/2014.
//  Copyright (c) 2014 Nathan Rima. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var sliderValue: UILabel!
    
    @IBOutlet weak var displayBgnd: UIImageView!
    
    @IBOutlet weak var displayMainName: UILabel!
    
    @IBOutlet weak var sliderPosition: UISlider!
    
    @IBAction func LEDValue(sender: UISlider) {
        sliderValue.text = "\(Int(sliderPosition.value))";
   
    }
    
    

}

