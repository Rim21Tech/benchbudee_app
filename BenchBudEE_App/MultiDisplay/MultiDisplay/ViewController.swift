//
//  ViewController.swift
//  MultiDisplay
//
//  Created by Rim21 on 25/09/2014.
//  Copyright (c) 2014 Nathan Rima. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var MetalBgnd: UIImageView!
    
    @IBOutlet weak var DisplayBgnd: UIImageView!
    
    @IBOutlet weak var AppTitleLabel: UILabel!
    
    @IBOutlet weak var SettingsLabel: UILabel!
    
    @IBOutlet weak var ADCBtn: UIButton!
    
    @IBOutlet weak var DACBtn: UIButton!
    
    
    @IBAction func buttonPressed(sender: UIButton) {
        var buttons = ADCBtn
        var button = DACBtn
            if (buttons == sender) {
                if (sender.selected){
                    buttons.selected = false
                    SettingsLabel.hidden = true
                    AppTitleLabel.hidden = false
                }
                else {
                    buttons.selected = true
                    SettingsLabel.hidden = false
                    AppTitleLabel.hidden = true
                    
                }
            }
            else {
                buttons.selected = false
                SettingsLabel.hidden = false
                AppTitleLabel.hidden = true
            }
        if (button == sender) {
            
                if (sender.selected){
                    button.selected = false
                    SettingsLabel.hidden = true
                    AppTitleLabel.hidden = false
                }
                else {
                    button.selected = true
                    SettingsLabel.hidden = false
                    AppTitleLabel.hidden = true
                    
                }
            }
            else {
                button.selected = false
                SettingsLabel.hidden = false
                AppTitleLabel.hidden = true
            }
        
        }

    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

