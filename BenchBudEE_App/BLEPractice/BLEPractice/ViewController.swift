//
//  ViewController.swift
//  BLEPractice
//
//  Created by Rim21 on 5/10/2014.
//  Copyright (c) 2014 Nathan Rima. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    @IBOutlet weak var BLERssi: UILabel!
    @IBOutlet weak var BLEResetting: UILabel!
    @IBOutlet weak var BLEUnautorised: UILabel!
    @IBOutlet weak var BLEUnknown: UILabel!
    @IBOutlet weak var BLEUnsupported: UILabel!
    
    
    var centralManager:CBCentralManager!
    var bluetoothReady = false
    var connectedDevice:CBPeripheral!

    override func viewDidLoad() {
        super.viewDidLoad()
        startUpCentralManager()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func startUpCentralManager() {
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager!) {
        switch(central.state) {
        case .PoweredOff:
            BLERssi.text = "Device powered off"
        case .PoweredOn:
            BLERssi.text = "Device ready"
            bluetoothReady = true
        case .Resetting:
            BLEResetting.text = "Device resetting"
        case .Unauthorized:
            BLEUnautorised.text = "Device not authorised"
        case .Unknown:
            BLEUnknown.text = "BLE state unknown"
        case .Unsupported:
            BLEUnsupported.text = "Device not supported"
        }
        
        if bluetoothReady {
            discoverDevices()
        }
    }
    
    func discoverDevices() {
        BLEUnautorised.text = "Searching"
        centralManager.scanForPeripheralsWithServices(nil, options: nil)
    }
    
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject : AnyObject]!, RSSI: NSNumber!) {
        BLERssi.text = RSSI.stringValue
        connectedDevice = peripheral
        peripheral.delegate = self
        centralManager.connectPeripheral(peripheral, options: nil)
    }
    
    
    func centralManager(central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!) {
        centralManager.stopScan()
        peripheral.discoverServices(nil)
        BLERssi.text = "connected: \(peripheral.name)"
    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverServices error: NSError!) {
        if peripheral.services != nil {
            for service in peripheral.services {
                BLEUnautorised.text = "Service Discovered: \(peripheral.identifier)"
            }
        } else {
            BLEUnautorised.text = "No Services"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

