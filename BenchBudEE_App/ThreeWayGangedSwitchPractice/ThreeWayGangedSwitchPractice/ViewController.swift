//
//  ViewController.swift
//  ThreeWayGangedSwitchPractice
//
//  Created by Rim21 on 20/09/2014.
//  Copyright (c) 2014 Nathan Rima. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var MetalBgnd: UIImageView!
    @IBOutlet weak var selectorBgnd: UIImageView!
    @IBOutlet weak var DACBtn: UIButton!
    @IBOutlet weak var LEDBtn: UIButton!
    @IBOutlet weak var ADCBtn: UIButton!
    
    
    @IBAction func selectorBtnPressed(sender: UIButton) {
        var buttons = [ADCBtn, LEDBtn, DACBtn]
        for button in buttons {
            if (button == sender) {
                if (sender.selected){
                    button.selected = false
                }
                else {
                    button.selected = true
                }
            }
            else {
                button.selected = false
            }
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

