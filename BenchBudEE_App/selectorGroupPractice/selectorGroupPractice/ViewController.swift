//
//  ViewController.swift
//  selectorGroupPractice
//
//  Created by Rim21 on 27/09/2014.
//  Copyright (c) 2014 Nathan Rima. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var BBBgnd: UIImageView!
    @IBOutlet weak var displayBgnd: UIImageView!
    @IBOutlet weak var selectorBgnd: UIImageView!
    
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var ADCTestLabel: UILabel!
    
    @IBOutlet weak var LEDTestLabel: UILabel!
    @IBOutlet weak var DACTestLabel: UILabel!
    
    @IBOutlet weak var ADCValue: UILabel!
    @IBOutlet weak var LEDValue: UILabel!
    @IBOutlet weak var DACValue: UILabel!
    
    @IBOutlet weak var ADCSliderDefault: UISlider!
    @IBOutlet weak var settingSlider: UISlider!
    @IBOutlet weak var LEDSlider: UISlider!
    @IBOutlet weak var DACSlider: UISlider!
    
    
    
    @IBOutlet weak var ADCBtn: UIButton!
    @IBOutlet weak var LEDBtn: UIButton!
    @IBOutlet weak var DACBtn: UIButton!
    
    @IBAction func selectorPressed(sender: UIButton) {
        var buttons = [ADCBtn, LEDBtn, DACBtn]
        for button in buttons {
            if (button == sender) {
                if (sender.selected){
                    button.selected = false
                    rangeLabel.hidden = true
                    settingsLabel.hidden = false
                    settingSlider.hidden = false
                    
                    if (sender == ADCBtn) {
                        ADCTestLabel.hidden = true
                        ADCSliderDefault.hidden = true
                        ADCValue.hidden = true
                    }
                    else if (sender == LEDBtn) {
                        LEDTestLabel.hidden = true
                        LEDSlider.hidden = true
                        LEDValue.hidden = true
                    }
                    else if (sender == DACBtn) {
                        DACTestLabel.hidden = true
                        DACSlider.hidden = true
                        DACValue.hidden = true
                    }
                }
                else {
                    button.selected = true
                    settingsLabel.hidden = true
                    rangeLabel.hidden = false
                    
                    if (sender == ADCBtn) {
                        ADCTestLabel.hidden = false
                        ADCSliderDefault.hidden = false
                        ADCValue.hidden = false
                        
                        LEDTestLabel.hidden = true
                        LEDSlider.hidden = true
                        LEDValue.hidden = true
                        
                        DACTestLabel.hidden = true
                        DACSlider.hidden = true
                        DACValue.hidden = true
                        
                        settingSlider.hidden = true
                    }
                    else if (sender == LEDBtn) {
                        LEDTestLabel.hidden = false
                        LEDSlider.hidden = false
                        LEDValue.hidden = false
                        
                        ADCTestLabel.hidden = true
                        ADCSliderDefault.hidden = true
                        ADCValue.hidden = true

                        
                        DACTestLabel.hidden = true
                        DACSlider.hidden = true
                        DACValue.hidden = true
                        
                        settingSlider.hidden = true
                    }
                    else if (sender == DACBtn) {
                        DACTestLabel.hidden = false
                        DACSlider.hidden = false
                        DACValue.hidden = false
                        
                        ADCTestLabel.hidden = true
                        ADCSliderDefault.hidden = true
                        ADCValue.hidden = true

                        
                        LEDTestLabel.hidden = true
                        LEDSlider.hidden = true
                        LEDValue.hidden = true
                        
                        settingSlider.hidden = true
                    }

                }
            }
            else {
                button.selected = false
            }
        }

    }
    
    @IBAction func ADCSlider(sender: UISlider) {
        ADCValue.text = "\(Int(ADCSliderDefault.value))"
    }
    
    @IBAction func LEDSliderUpdate(sender: UISlider) {
        LEDValue.text = "\(Int(LEDSlider.value))"
    }

    @IBAction func DACSliderUpdate(sender: UISlider) {
        DACValue.text = "\(Int(DACSlider.value))"
    }
    

    override func viewDidLoad() {
        
        configureADCSliderDefault()
        configuresettingSlider()
        configureLEDSlider()
        configureDACSlider()
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func configureADCSliderDefault() {
        let leftTrackImage = UIImage(named: "sliderMaxMin")
        ADCSliderDefault.setMinimumTrackImage(leftTrackImage, forState: .Normal)
        
        let rightTrackImage = UIImage(named: "sliderMaxMin")
        ADCSliderDefault.setMaximumTrackImage(rightTrackImage, forState: .Normal)
        
        let thumbImage = UIImage(named: "sliderBtn")
        ADCSliderDefault.setThumbImage(thumbImage, forState: .Normal)
        
        ADCSliderDefault.minimumValue = 1
        ADCSliderDefault.maximumValue = 255
        ADCSliderDefault.continuous = true
        ADCSliderDefault.value = 1
    }
    
    func configuresettingSlider() {
        let leftTrackImage = UIImage(named: "sliderMaxMin")
        settingSlider.setMinimumTrackImage(leftTrackImage, forState: .Normal)
        
        let rightTrackImage = UIImage(named: "sliderMaxMin")
        settingSlider.setMaximumTrackImage(rightTrackImage, forState: .Normal)
        
        let thumbImage = UIImage(named: "sliderBtn")
        settingSlider.setThumbImage(thumbImage, forState: .Normal)
        
        settingSlider.minimumValue = 1
        settingSlider.maximumValue = 255
        settingSlider.continuous = true
        settingSlider.value = 1
    }
    
    func configureLEDSlider() {
        let leftTrackImage = UIImage(named: "sliderMaxMin")
        LEDSlider.setMinimumTrackImage(leftTrackImage, forState: .Normal)
        
        let rightTrackImage = UIImage(named: "sliderMaxMin")
        LEDSlider.setMaximumTrackImage(rightTrackImage, forState: .Normal)
        
        let thumbImage = UIImage(named: "sliderBtn")
        LEDSlider.setThumbImage(thumbImage, forState: .Normal)
        
        LEDSlider.minimumValue = 1
        LEDSlider.maximumValue = 255
        LEDSlider.continuous = true
        LEDSlider.value = 1
    }
    
    func configureDACSlider() {
        let leftTrackImage = UIImage(named: "sliderMaxMin")
        DACSlider.setMinimumTrackImage(leftTrackImage, forState: .Normal)
        
        let rightTrackImage = UIImage(named: "sliderMaxMin")
        DACSlider.setMaximumTrackImage(rightTrackImage, forState: .Normal)
        
        let thumbImage = UIImage(named: "sliderBtn")
        DACSlider.setThumbImage(thumbImage, forState: .Normal)
        
        DACSlider.minimumValue = 1
        DACSlider.maximumValue = 255
        DACSlider.continuous = true
        DACSlider.value = 1
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

