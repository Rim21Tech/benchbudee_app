//
//  ViewController.swift
//  customSliderProject
//
//  Created by Rim21 on 14/09/2014.
//  Copyright (c) 2014 Nathan Rima. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var sliderBgnd: UIImageView!
    
    @IBOutlet weak var customSlider: UISlider!
    
    @IBOutlet weak var wheelBgnd: UIImageView!
    
    @IBOutlet weak var wheelSlider: UISlider!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCustomSlider()
        configureCustomWheel()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    func configureCustomSlider() {
        let leftTrackImage = UIImage(named: "sliderMaxMin")
        customSlider.setMinimumTrackImage(leftTrackImage, forState: .Normal)
        
        let rightTrackImage = UIImage(named: "sliderMaxMin")
        customSlider.setMaximumTrackImage(rightTrackImage, forState: .Normal)
        
        let thumbImage = UIImage(named: "sliderBtn")
        customSlider.setThumbImage(thumbImage, forState: .Normal)
        
        customSlider.minimumValue = 1
        customSlider.maximumValue = 255
        customSlider.continuous = false
        customSlider.value = 84
    }

    func configureCustomWheel() {
        let leftTrackImage = UIImage(named: "SliderMinMax")
        wheelSlider.setMinimumTrackImage(leftTrackImage, forState: .Normal)
        
        let rightTrackImage = UIImage(named: "SliderMinMax")
        wheelSlider.setMaximumTrackImage(rightTrackImage, forState: .Normal)
        
        let thumbImage = UIImage(named: "sliderBtn")
        wheelSlider.setThumbImage(thumbImage, forState: .Normal)
        
        wheelSlider.minimumValue = 1
        wheelSlider.maximumValue = 255
        wheelSlider.continuous = false
        wheelSlider.value = 84
    }
    
    @IBAction func customSliderValue(sender: UISlider) {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

