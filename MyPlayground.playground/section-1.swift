// Playground - noun: a place where people can play

import UIKit

let testLabel = UILabel(frame: CGRectMake(0, 0, 120, 40))
testLabel.text = "Hello, Swift!"
testLabel.backgroundColor = UIColor(red: 0.9, green: 0.2, blue: 0.2, alpha: 1.0)
testLabel.textAlignment = NSTextAlignment.Center
testLabel.layer.masksToBounds = true
testLabel.layer.cornerRadius = 10

testLabel
